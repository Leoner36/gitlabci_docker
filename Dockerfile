FROM node as builder
RUN mkdir /app
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build
FROM nginx
EXPOSE 80
COPY --from=builder /app/build /usr/share/nginx/html
RUN ls /usr/share/nginx/html
